// Ham Khong Co Tham So
function sayHello(){
    console.log("Hello, have a great day!")
}
sayHello();
// Ham Co Tham So
function sayHi(name){
    console.log("Hi " + name);
}
var userName = "Bob"
sayHi(userName);
sayHi("Micheal");

function tinhDTB (toan, van){
    var dtb =  (toan + van)/2;
    console.log("Diem TB: ", dtb);
    return dtb;
}

var diemToan =  2;
var diemVan = 4;
var score = tinhDTB(diemToan, diemVan);
console.log("Score: ", score);

// Function Literal

var sayGoodbye = function(){
    console.log("Good Bye!");
};
sayGoodbye();