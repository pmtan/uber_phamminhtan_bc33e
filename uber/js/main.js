const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";

function tinhGiaTienKmDauTien(carType){
    if (carType == UBER_CAR){
        return 8000;
    } else if (carType == UBER_SUV) {
        return 9000;
    } else if (carType == UBER_BLACK) {
        return 10000;
    } else {
        return 0;
    }
}

function tinhGiaTien19Km(carType){
    switch(carType){
        case UBER_CAR:
            return 7500;
        case UBER_SUV:
            return 8500;
        case UBER_BLACK:
            return 9500;
        default:
            return 0;
    }
}

function tinhGiaTien19KmTroLen(carType){
    switch(carType){
        case UBER_CAR:
            return 7000;
        case UBER_SUV:
            return 8000;
        case UBER_BLACK:
            return 9000;
        default:
            return 0;
    }
}

function tinhTien(carType,soKm){
    var tongTien = 0;
    if (soKm <=1){
        tongTien = tinhGiaTienKmDauTien(carType)
    } else if(soKm <20){
        tongTien = tinhGiaTienKmDauTien(carType) + tinhGiaTien19Km(carType)*(soKm -1);
    } else {
        tongTien = tinhGiaTienKmDauTien(carType) + tinhGiaTien19Km(carType)*18 + tinhGiaTien19KmTroLen(carType)*(soKm - 19);
    }
    return tongTien;
}
// Main Function
function tinhTienUber() {
    var uberType= document.querySelector('input[name="selector"]:checked').value;
    console.log(uberType);
    var kilomet = document.getElementById("txt-km").value*1;
    // var giaTienKmDauTien = tinhGiaTienKmDauTien(uberType);
    // console.log("Gia tien Km Dau Tien: ", giaTienKmDauTien);
    // var giaTien19Km = tinhGiaTien19Km(uberType);
    // console.log("Gia Tien 19 Km: ", giaTien19Km);
    // var giaTien19KmTroLen = tinhGiaTien19KmTroLen(uberType);
    // console.log("Gia Tien 19 Km Tro Len: ", giaTien19KmTroLen);
    var tongSoTien = tinhTien(uberType,kilomet);
    console.log("TongSoTien: ",tongSoTien);
    document.getElementById("result").innerHTML = `<p> Tong So Tien: ${tongSoTien} VND`;
}


